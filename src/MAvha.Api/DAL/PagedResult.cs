﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAvha.Api.DAL
{
    public class PagedResult<T>
    {
        public IEnumerable<T> Records { get; private set; }
        public int Page { get; private set; }
        public int PageSize { get; private set; }
        public int TotalCount { get; private set; }

        public PagedResult(IEnumerable<T> Records, int Page, int PageSize, int TotalCount)
        {
            this.Records = Records;
            this.Page = Page;
            this.PageSize = PageSize;
            this.TotalCount = TotalCount;
        }
    }
}
