﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAvha.Api.DAL.Extensions
{
    public static class QueryableExtensions
    {
        public static async Task<PagedResult<TModel>> GetPagedAsync<TModel>(this IQueryable<TModel> query, int? page = 1, int? pageSize = 20)
        {
            if (page == null || page <= 0)
            {
                page = 1;
            }
            if (pageSize == null || pageSize <= 0)
            {
                pageSize = 20;
            }

            var resultsToSkip = (page.Value - 1) * pageSize.Value;
            var totalCount = query.Count();

            return new PagedResult<TModel>(await query.Skip(resultsToSkip).Take(pageSize.Value).ToListAsync(), page.Value, pageSize.Value, totalCount);
        }
    }
}
