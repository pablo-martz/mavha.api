﻿using MAvha.Api.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAvha.Api.DAL
{
    public class MAvhaContext : DbContext
    {
        public MAvhaContext(DbContextOptions<MAvhaContext> options)
            : base(options)
        { }

        public DbSet<Todo> Todos { get; set; }
        public DbSet<Attachment> Attachments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Todo>()
                .HasIndex(x => x.Description);
            modelBuilder.Entity<Todo>()
                .HasIndex(x => new { x.Description, x.State });
            modelBuilder.Entity<Attachment>()
                .HasIndex(x => x.Filename);
        }
    }
}
