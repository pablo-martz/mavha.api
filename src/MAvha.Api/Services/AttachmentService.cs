﻿using MAvha.Api.DAL;
using MAvha.Api.Models;
using MAvha.Api.Models.Exceptions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAvha.Api.Services
{
    public class AttachmentService
    {
        private readonly ILogger<AttachmentService> _logger;
        private readonly MAvhaContext _context;

        public AttachmentService(ILogger<AttachmentService> logger, MAvhaContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task<Attachment> GetByIdAsync(int Id)
        {
            return await _context.Attachments.AsNoTracking().SingleOrDefaultAsync(t => t.Id == Id);
        }

        public async Task<Attachment> CreateAsync(Attachment model)
        {
            // Check for errors in the model to save
            var errors = this.GetErrorsFromModel(model);
            if (errors.Any())
            {
                throw new InvalidModelException(errors);
            }

            // Add to DB
            await _context.Attachments.AddAsync(model);
            await _context.SaveChangesAsync();

            return model;
        }

        private List<string> GetErrorsFromModel(Attachment model)
        {
            var errors = new List<string>();

            // File size
            if (model.File.Length > 26214400) //25MB
            {
                errors.Add("The file size limit is 25MB");
            }

            return errors;
        }
    }
}
