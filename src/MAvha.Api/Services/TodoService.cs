﻿using MAvha.Api.DAL;
using MAvha.Api.DAL.Extensions;
using MAvha.Api.Models;
using MAvha.Api.Models.Exceptions;
using MAvha.Api.ViewModels;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAvha.Api.Services
{
    public class TodoService
    {
        private readonly ILogger<TodoService> _logger;
        private readonly MAvhaContext _context;

        public TodoService(ILogger<TodoService> logger, MAvhaContext context)
        {
            _logger = logger;
            _context = context;
        }

        public async Task<TodoViewModel> GetByIdAsync(int Id)
        {
            return await _context.Todos
                .Include(t => t.Attachment)
                .AsNoTracking()
                .Select(t => new TodoViewModel
                {
                    Id = t.Id,
                    Description = t.Description,
                    State = t.State,
                    Attachment = t.Attachment == null ? null : new AttachmentViewModel
                    {
                        Id = t.Attachment.Id,
                        Filename = t.Attachment.Filename
                    }
                })
                .SingleOrDefaultAsync(t => t.Id == Id);
        }

        public async Task<PagedResult<TodoViewModel>> GetPagedAsync(string searchTerm, TodoState state, bool? hasAttachment, int page, int pageSize, string orderBy, bool orderDesc)
        {
            var query = _context.Todos.Include(t => t.Attachment).AsQueryable();

            // Filter
            if (!string.IsNullOrWhiteSpace(searchTerm))
            {
                searchTerm = searchTerm.Trim();

                int idSearchTerm;
                int.TryParse(searchTerm, out idSearchTerm);

                query = query.Where(t => t.Id.Equals(idSearchTerm)
                || t.Description.Contains(searchTerm)
                || (t.Attachment.Filename.Contains(searchTerm)));
            }

            if (state > 0)
            {
                query = query.Where(t => t.State == state);
            }

            if (hasAttachment != null)
            {
                query = query.Where(t => (t.Attachment != null) == hasAttachment);
            }

            // Order
            if (string.IsNullOrWhiteSpace(orderBy))
            {
                orderBy = nameof(Todo.Id);
            }
            IOrderedQueryable<Todo> orderedQuery;
            if (orderBy.Equals(nameof(Todo.Id), StringComparison.InvariantCultureIgnoreCase))
            {
                orderedQuery = orderDesc ? query.OrderByDescending(t => t.Id) : query.OrderBy(t => t.Id);
            }
            else if (orderBy.Equals(nameof(Todo.Description), StringComparison.InvariantCultureIgnoreCase))
            {
                orderedQuery = orderDesc ? query.OrderByDescending(t => t.Description) : query.OrderBy(t => t.Description);
            }
            else if (orderBy.Equals(nameof(Todo.State), StringComparison.InvariantCultureIgnoreCase))
            {
                orderedQuery = orderDesc ? query.OrderByDescending(t => t.State) : query.OrderBy(t => t.State);
            }
            else if (orderBy.Equals(nameof(Todo.Attachment), StringComparison.InvariantCultureIgnoreCase))
            {
                orderedQuery = orderDesc ? query.OrderByDescending(t => t.Attachment == null ? "" : t.Attachment.Filename) : query.OrderBy(t => t.Attachment == null ? "" : t.Attachment.Filename);
            }
            else
            {
                orderedQuery = orderDesc ? query.OrderByDescending(t => t.Id) : query.OrderBy(t => t.Id);
            }

            // Convert to ViewModel (this could be improved, using an object mapper)
            var queryToExecute = orderedQuery.AsNoTracking()
                .Select(t => new TodoViewModel
                {
                    Id = t.Id,
                    Description = t.Description,
                    State = t.State,
                    Attachment = t.Attachment == null ? null : new AttachmentViewModel
                    {
                        Id = t.Attachment.Id,
                        Filename = t.Attachment.Filename
                    }
                });

            // Return paginated results
            return await queryToExecute.GetPagedAsync(page, pageSize);
        }

        public async Task<Todo> CreateAsync(Todo model)
        {
            // Check for errors in the model to save
            var errors = await this.GetErrorsFromModelAsync(model);
            if (errors.Any())
            {
                throw new InvalidModelException(errors);
            }

            // Add to DB and don't try to insert the Attachment (has already been inserted)
            await _context.Todos.AddAsync(model);
            _context.Entry(model.Attachment).State = EntityState.Unchanged;

            await _context.SaveChangesAsync();

            return model;
        }

        public async Task UpdateAsync(Todo model)
        {
            // Check for errors in the model to save
            var errors = await GetErrorsFromModelAsync(model);
            if (errors.Any())
            {
                throw new InvalidModelException(errors);
            }

            // Get the entity from the DB
            var dbEntity = await _context.Todos.FindAsync(model.Id);
            if (dbEntity == null)
            {
                throw new ModelNotFoundException();
            }

            // Load the Attachment
            await _context.Entry(dbEntity).Reference(x => x.Attachment).LoadAsync();

            dbEntity.Description = model.Description;
            dbEntity.State = model.State;

            // If Attachment changed
            if (model.Attachment?.Id != dbEntity.Attachment?.Id)
            {
                // Delete the previous attachment
                if (dbEntity.Attachment != null)
                {
                    _context.Attachments.Remove(dbEntity.Attachment);
                }

                // If there's an attachment to add, then add it
                if (model.Attachment != null)
                {
                    dbEntity.Attachment = new Attachment { Id = model.Attachment.Id };
                    _context.Entry(dbEntity.Attachment).State = EntityState.Unchanged;
                }
            }

            await _context.SaveChangesAsync();
        }

        public async Task<Todo> DeleteAsync(int Id)
        {
            // Get the entity from the DB
            var dbEntity = await _context.Todos.FindAsync(Id);
            if (dbEntity == null)
            {
                return null;
            }

            // Load the Attachment
            await _context.Entry(dbEntity).Reference(x => x.Attachment).LoadAsync();
            // Delete the attachment
            if (dbEntity.Attachment != null)
            {
                _context.Attachments.Remove(dbEntity.Attachment);
            }

            // Delete
            _context.Todos.Remove(dbEntity);
            await _context.SaveChangesAsync();

            return dbEntity;
        }

        private async Task<List<string>> GetErrorsFromModelAsync(Todo model)
        {
            var errors = new List<string>();

            // Description is required
            if (string.IsNullOrWhiteSpace(model.Description))
            {
                errors.Add("A description is required");
            }

            // Description is required
            if (model.Description.Length > 500)
            {
                errors.Add("The description cannot exceed 500 characters");
            }

            // No 2 tasks with the same Description
            if (await _context.Todos.AsNoTracking().AnyAsync(t => t.Id != model.Id && t.Description == model.Description))
            {
                errors.Add("There is another to-do task with the same Description");
            }

            return errors;
        }
    }
}
