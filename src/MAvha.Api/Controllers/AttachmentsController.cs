﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MAvha.Api.DAL;
using MAvha.Api.Models;
using MAvha.Api.Models.Exceptions;
using MAvha.Api.Services;
using MAvha.Api.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Net.Http.Headers;

namespace MAvha.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AttachmentsController : ControllerBase
    {
        private readonly ILogger<AttachmentsController> _logger;
        private readonly AttachmentService _attachmentService;

        public AttachmentsController(ILogger<AttachmentsController> logger, AttachmentService attachmentService)
        {
            _logger = logger;
            _attachmentService = attachmentService;
        }

        // GET api/Attachments/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            var attachment = await _attachmentService.GetByIdAsync(id);

            if (attachment == null)
            {
                return NotFound();
            }

            return File(attachment.File, "application/octet-stream", attachment.Filename);
        }

        // POST: api/Attachments
        [HttpPost]
        public async Task<IActionResult> Post([FromForm]Microsoft.AspNetCore.Http.IFormFileCollection file)
        {
            //var file = files.GetFile("file");
            //           var file = HttpContext.Request.Form.Files.Count > 0 ? HttpContext.Request.Form.Files[0] : null;
            AttachmentViewModel viewModel;
            if (file.Count > 0)
            {
                var attachment = new Attachment()
                {
                    Filename = file.First().FileName
                };
                using (var ms = new System.IO.MemoryStream())
                {
                    file.First().OpenReadStream().CopyTo(ms);
                    attachment.File = ms.ToArray();
                }

                try
                {
                    await _attachmentService.CreateAsync(attachment);
                    viewModel = new AttachmentViewModel { Id = attachment.Id, Filename = attachment.Filename };
                }
                catch (InvalidModelException ex)
                {
                    _logger.LogError(ex, ex.Message);
                    return BadRequest(ex.Errors);
                }

                return CreatedAtAction("Get", new { id = attachment.Id }, new AttachmentViewModel { Id = attachment.Id, Filename = attachment.Filename });
            }
            return BadRequest();
        }
    }
}
