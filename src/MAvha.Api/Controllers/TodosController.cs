﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MAvha.Api.DAL;
using MAvha.Api.Models;
using MAvha.Api.Models.Exceptions;
using MAvha.Api.Services;
using MAvha.Api.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace MAvha.Api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TodosController : ControllerBase
    {
        private readonly ILogger<TodosController> _logger;
        private readonly TodoService _todoService;

        public TodosController(ILogger<TodosController> logger, TodoService todoService)
        {
            _logger = logger;
            _todoService = todoService;
        }

        // GET api/Todos
        [HttpGet]
        public async Task<PagedResult<TodoViewModel>> GetAsync(string searchTerm, TodoState state, bool? hasAttachment, int page, int pageSize, string orderBy, bool orderDesc)
        {
            return await _todoService.GetPagedAsync(searchTerm, state, hasAttachment, page, pageSize, orderBy, orderDesc);
        }

        // GET api/Todos/5
        [HttpGet("{id}")]
        public async Task<IActionResult> GetAsync(int id)
        {
            var todo = await _todoService.GetByIdAsync(id);

            if (todo == null)
            {
                return NotFound();
            }

            return Ok(todo);
        }

        // POST: api/Todos
        [HttpPost]
        public async Task<IActionResult> Post([FromBody] Todo todo)
        {
            try
            {
                await _todoService.CreateAsync(todo);
            }
            catch (InvalidModelException ex)
            {
                _logger.LogError(ex, ex.Message);
                return BadRequest(ex.Errors);
            }

            return CreatedAtAction("Get", new { id = todo.Id }, todo);
        }

        // PUT: api/Todos/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put([FromRoute] int id, [FromBody] TodoViewModel todo)
        {
            if (id != todo.Id)
            {
                return BadRequest();
            }

            try
            {
                var model = new Todo { Id = todo.Id, Description = todo.Description, State = todo.State };
                var attachmentModel = todo.Attachment == null ? null : new Attachment { Id = todo.Attachment.Id, Filename = todo.Attachment.Filename };
                model.Attachment = attachmentModel;

                await _todoService.UpdateAsync(model);
            }
            catch (InvalidModelException ime)
            {
                _logger.LogError(ime, ime.Message);
                return BadRequest(ime.Errors);
            }
            catch (ModelNotFoundException ex)
            {
                _logger.LogError(ex, ex.Message);
                return NotFound();
            }

            return NoContent();
        }

        // DELETE: api/Todos/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete([FromRoute] int id)
        {
            Todo todo = await _todoService.DeleteAsync(id);
            if (todo == null)
            {
                return NoContent();
            }

            return Ok(todo);
        }
    }
}
