﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAvha.Api.Models
{
    public class Attachment
    {
        public int Id { get; set; }
        public string Filename { get; set; }
        public byte[] File { get; set; }
    }
}
