﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAvha.Api.Models.Exceptions
{
    public class ModelNotFoundException: Exception
    {
        public ModelNotFoundException()
            : base()
        {

        }

        public ModelNotFoundException(string message)
            : base(message)
        {

        }
    }
}
