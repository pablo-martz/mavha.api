﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAvha.Api.Models.Exceptions
{
    public class InvalidModelException : Exception
    {
        public IEnumerable<string> Errors { get; set; }

        public InvalidModelException(IEnumerable<string> errors)
            : base()
        {
            Errors = errors;
        }

        public InvalidModelException(string message, IEnumerable<string> errors)
            : base(message)
        {
            Errors = errors;
        }
    }
}
