using System;

namespace MAvha.Api.Models
{
    public class Todo
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public TodoState State { get; set; }

        public Attachment Attachment { get; set; }
    }

    public enum TodoState : byte
    {
        Pending = 1,
        Resolved
    }
}
