﻿using MAvha.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAvha.Api.ViewModels
{
    public class TodoViewModel
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public TodoState State { get; set; }

        public AttachmentViewModel Attachment { get; set; }
    }
}
