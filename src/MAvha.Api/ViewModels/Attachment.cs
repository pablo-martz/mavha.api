﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MAvha.Api.ViewModels
{
    public class AttachmentViewModel
    {
        public int Id { get; set; }
        public string Filename { get; set; }
    }
}
