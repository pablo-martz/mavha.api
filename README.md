
# README

This is a [.NET Core 3.1]([https://devblogs.microsoft.com/dotnet/announcing-net-core-3-1/](https://devblogs.microsoft.com/dotnet/announcing-net-core-3-1/)) API for the MAvha Tech Challenge (Back-end for TODO's management).

## How do I get set up?

* Make sure the front-end domain is included in the CORS configuration in `Startup.cs`:
```cs
options.AddDefaultPolicy(
	builder =>
	{
	    builder.WithOrigins("http://localhost:4200",
	                        "https://localhost:4200");
	    builder.AllowAnyMethod();
	    builder.AllowAnyHeader();
	});
```

* Database configuration
Configure the connection string to the DB in appsettings.json (by default it will use a local instance of SQL Server):
```json
"ConnectionStrings": {
"MAvha": "Server=.;Database=MAvha;Trusted_Connection=True;Application Name=MAvha.Api"
}
```
* Make sure to use the configured application Url from `launchSettings.json` in the environment files from the front-end.

* Run the following scripts in your instance of SQL Server:
[schema.sql](https://bitbucket.org/pablo-martz/mavha.api/src/master/DB/schema.sql): To create a new database with all the necessary objects.
[data.sql](https://bitbucket.org/pablo-martz/mavha.api/src/master/DB/data.sql): To populate the Todos table with 1M records for testing.

## Who do I talk to?

* Pablo Martinez <<pablohernan.martinez@gmail.com>>